<?php

namespace Lvzmen\Helper;

class iFormatHelper
{
    /**
     * 返回浮点型数据
     * @param $value
     * @param int $decimals
     * @return float|int
     */
    public static function float($value, $decimals = 2)
    {
        $value = is_string($value) ? trim($value) : $value;
        return number_format($value, $decimals, '.', '') * 1;
    }

    /**
     * 去除百分号
     * $str = '73%';
     * self::remotePercent($str);
     * result: 0.73
     *
     * @param $str
     * @return float|int
     */
    public static function remotePercent($str)
    {
        return self::float(str_replace('%', '', $str) / 100, 4);
    }


    /**
     * 脱敏
     */
    public static function unSensible($data = [], $column = [])
    {
        try {
            foreach ($data as $key => $item) {
                if (is_array($item)) {
                    $data[$key] = self::unSensible($item, $column);
                } else {
                    if (in_array($key, $column)) {
                        if (strpos($item, '@') !== false) {
                            // 先处理邮箱：chenzhiwei@hzanchu.com => che***@hzanchu.com
                            $arr = explode('@', $item);
                            $data[$key] = mb_substr($arr[0], 0, 3) . '***' . '@' . $arr[1];
                        } elseif (strlen($item) > 7 && substr($item, 0, 1) == '0') {
                            // 固化号码 0578-3261861 => 0578****1861
                            $data[$key] = substr($item, 0, 4) . '****' . substr($item, -4);
                        } elseif (strlen($item) > 10) {
                            // 处理手机号码 和 身份证
                            $data[$key] = substr($item, 0, 3) . '****' . substr($item, -4);
                        } else {
                            // 处理短号：6261861
                            $data[$key] = '***' . substr($item, -4);
                        }
                    }
                }
            }
            return $data;
        } catch (\Exception $e) {
            var_dump($e->getMessage(), $e->getLine());
            die;
        }

    }

    /**
     * 驼峰转换为下划线
     *
     * $result = self::unCamelize('GetList');
     * $result：get_list
     *
     * @param $data
     * @return mixed
     */
    public static function unCamelize($camelCaps, $separator = '_')
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $camelCaps));
    }

    /**
     * 下划线转换为驼峰
     *
     * $result = self::camelize('get_list');
     * $result：GetList
     *
     * @param $data
     * @return mixed
     */
    public static function camelize($camelCaps, $separator = '_')
    {
        $items = explode($separator, $camelCaps);
        $result = '';
        foreach ($items as $key => $item) {
            $result .= ucfirst(strtolower($item));
        }
        return $result;
    }

    /**
     * 仅用于Laravel的通用路由设置
     *
     * Route::get("{controller}/{action}", FormatUtil::route());
     * 如果访问的路由为/api/farm/index，则上面的配置同等于
     * Route::get("farm/index", FarmController::index());
     *
     * @return string
     */
    public static function route()
    {
        $url = explode('/', request()->url());
        $action = array_pop($url);
        $controller = array_pop($url);
        $controller = self::camelize($controller) . 'Controller';
        $action = self::camelize($action);
        $action = lcfirst($action);
        $url = $controller . '@' . $action;
        return $url;
    }

    /**
     * 仅用于Laravel的通用路由设置
     *
     * Route::get("{controller}/{action}", FormatUtil::routeCommon());
     * 如果访问的路由为/api/farm/index，则上面的配置同等于
     * Route::get("farm/index", CommonController::index());
     *
     * @return string
     */
    public static function routeCommon()
    {
        $url = explode('/', request()->url());
        $action = array_pop($url);
        if (is_numeric($action)) {
            $action = array_pop($url);
        }
        $controller = 'CommonController';
        $action = self::camelize($action);
        $action = lcfirst($action);
        $url = $controller . '@' . $action;
        return $url;
    }
}
