<?php

namespace Lvzmen\Helper;

class iDateHelper
{
    /**
     * 获取某个日期的偏移日期
     *
     * example:
     *
     * 1、获取前天的日期
     * $theDayBeforeYesterday = self::offset(-2);
     *
     * 2、获取去年这个时候的日期：
     * $date = self::offset(-1, 'year')
     *
     * @param int $offset
     * @param string $unit
     * @param string $format
     * @param string $date
     * @return false|string
     */
    public static function offset($offset = 0, $unit = 'day', $format = 'Y-m-d', $date = '')
    {
        $date = $date == '' ? date('Y-m-d') : $date;
        $calc = $offset > 0 ? '+' : '';
        return date($format, strtotime("$date $calc $offset $unit"));
    }

    /**
     * 递归查找某个字段，并格式化为目标格式
     * @param array $array
     * @param array $columns
     * @param string $format
     * @return array|mixed
     */
    public static function format($array = [], $columns = [], $format = "Y.m")
    {
        foreach ($array as $key => $item) {
            if (!is_array($item) && in_array($key, $columns, true)) {
                $array[$key] = date($format, strtotime($item));
            }
            if (is_array($item)) {
                $array[$key] = self::format($item, $columns, $format);
            }
        }
        return $array;
    }
}
